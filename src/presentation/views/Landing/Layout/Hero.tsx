

export const Hero = () => {


  return (
    <main>
      <div>
        <div>
        <img src="https://framerusercontent.com/images/0zdTtxxiDzMeScfXQjnaHaoqXUM.png" alt="" srcSet="https://framerusercontent.com/images/0zdTtxxiDzMeScfXQjnaHaoqXUM.png?scale-down-to=512 512w, https://framerusercontent.com/images/0zdTtxxiDzMeScfXQjnaHaoqXUM.png?scale-down-to=1024 1024w, https://framerusercontent.com/images/0zdTtxxiDzMeScfXQjnaHaoqXUM.png?scale-down-to=2048 2048w, https://framerusercontent.com/images/0zdTtxxiDzMeScfXQjnaHaoqXUM.png 2880w" sizes="calc((min(100vw, 1480px) - 80px) * 1.0286)"   style={{
    display: 'block',
    width: '100%',
    height: '100%',
    borderRadius: 'inherit',
    objectPosition: 'center',
    objectFit: 'cover',
    imageRendering: 'auto',
  }} />
        </div>
      </div>
    </main>
  )
}
